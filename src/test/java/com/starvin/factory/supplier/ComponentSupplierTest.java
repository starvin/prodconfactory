package com.starvin.factory.supplier;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.starvin.factory.conveyerbelt.ComponentConveyerBelt;
import com.starvin.factory.supplier.ComponentSupplier;
import com.starvin.factory.supplier.component.Component;

public class ComponentSupplierTest {

	ComponentConveyerBelt conveyorBelt;
	ComponentSupplier componentSupplier;
	
	@Before
	public void setUp() throws Exception {
		 conveyorBelt = new ComponentConveyerBelt();
		 conveyorBelt.crankItUp();
		 try{
			 componentSupplier = new ComponentSupplier(conveyorBelt);
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
	}

	@Test
	public void testComponentSupplier() {
		
		assertNotNull(componentSupplier);
		assertNotNull(componentSupplier.getConveyorBelt());
	}
	
	@Test
	public void testGetRandomComponent() {
		
		for (int i =0; i <100; i++){
			Component component = componentSupplier.getRandomComponent();
			assertNotNull(component);
		}
	}
}
