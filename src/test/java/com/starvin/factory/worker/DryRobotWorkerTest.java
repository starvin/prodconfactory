package com.starvin.factory.worker;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.starvin.factory.conveyerbelt.ComponentConveyerBelt;
import com.starvin.factory.supplier.component.BroomComponent;
import com.starvin.factory.supplier.component.Component;
import com.starvin.factory.supplier.component.MainComponent;
import com.starvin.factory.worker.DryRobotWorker;

public class DryRobotWorkerTest {

	DryRobotWorker dryRobotWorker;
	ComponentConveyerBelt conveyorBelt;
	
	@Before
	public void setUp() throws Exception {
		 conveyorBelt = new ComponentConveyerBelt();
		 conveyorBelt.crankItUp();
		
		 dryRobotWorker = new DryRobotWorker(conveyorBelt);
	}

	
	@Test
	public void testStartWorking(){
		Component main = new MainComponent("MAIN");
		Component broom1 = new BroomComponent("BROOM");
		Component broom2 = new BroomComponent("BROOM");
		
		conveyorBelt.placeItem(main);
		conveyorBelt.placeItem(broom1);
		conveyorBelt.placeItem(broom2);
		
		
		dryRobotWorker.startWorking();

		assertTrue(dryRobotWorker.noOfRobotsAssembled == 1);	
	}
	
	/**
	 * Test no dry robot created when incorrect components on conveyer belt.
	 */
	@Test
	public void testStartWorkingFalse(){
		Component main = new MainComponent("MAIN");
		Component broom1 = new BroomComponent("BROOM");
		Component mop = new BroomComponent("MOP");
		
		conveyorBelt.placeItem(main);
		conveyorBelt.placeItem(broom1);
		conveyorBelt.placeItem(mop);
		
		dryRobotWorker.startWorking();
		
		assertTrue(dryRobotWorker.noOfRobotsAssembled == 0);	
	}
}
