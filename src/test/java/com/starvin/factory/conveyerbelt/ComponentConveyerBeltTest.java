package com.starvin.factory.conveyerbelt;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.starvin.factory.conveyerbelt.ComponentConveyerBelt;
import com.starvin.factory.supplier.component.Component;
import com.starvin.factory.supplier.component.MainComponent;

public class ComponentConveyerBeltTest {

	ComponentConveyerBelt componentConveyerBelt;
	
	@Before
	public void setUp() throws Exception {
		
		componentConveyerBelt = new ComponentConveyerBelt();
	}

	@Test
	public void testCrankItUp() {
		componentConveyerBelt.crankItUp();
		
		assertNotNull(componentConveyerBelt.getQueue());
	}

	@Test
	public void removeLastItem() {
		
		 Component component = (Component) componentConveyerBelt.removeLastItem();
		 
		 assertNotNull(component);
	}

	@Test
	public void testPlaceItem() {
		Component component = new MainComponent("MAIN");
		
		componentConveyerBelt.placeItem(component);
			
		assertTrue(componentConveyerBelt.getQueue().size() >0);
	}

	@Test
	public void testRemoveLastItem() {
		fail("Not yet implemented");
	}

	@Test
	public void testCheckItem() {
		fail("Not yet implemented");
	}

}
