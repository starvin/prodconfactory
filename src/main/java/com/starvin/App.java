package com.starvin;

import com.starvin.factory.RobotFactory;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "_________Factory open for business_________" );
        
        RobotFactory robotFactory = new RobotFactory("Acme");
    	
    	robotFactory.startFactory();
    	 
    }
}