package com.starvin.factory;


import com.starvin.factory.conveyerbelt.ComponentConveyerBelt;
import com.starvin.factory.supplier.ComponentSupplier;
import com.starvin.factory.worker.DryRobotWorker;
import com.starvin.factory.worker.WetRobotWorker;

public class RobotFactory extends AbstractFactory{

	ComponentConveyerBelt conveyerBelt;
	ComponentSupplier componentSupplier;
	DryRobotWorker dryRobotWorker;
	WetRobotWorker wetRobotWorker;
	
	public RobotFactory(String factoryName){
		super.factoryName = factoryName;
	}
	
	public void startFactory(){
		
		conveyerBelt = new ComponentConveyerBelt();
		conveyerBelt.crankItUp();
		
		componentSupplier = new ComponentSupplier(conveyerBelt);
		componentSupplier.startSupplying();
		
		dryRobotWorker = new DryRobotWorker(conveyerBelt);
		dryRobotWorker.startWorking();
		
		wetRobotWorker = new WetRobotWorker(conveyerBelt);
		wetRobotWorker.startWorking();
		
	}
	
	@Override
	public void closeFactory() {
		conveyerBelt.shutItDown();
	}
}
