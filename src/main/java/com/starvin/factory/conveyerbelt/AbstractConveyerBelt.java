package com.starvin.factory.conveyerbelt;

import java.util.Queue;

import com.starvin.factory.supplier.component.Component;

public abstract class AbstractConveyerBelt {

	protected Queue<Object> queue;
	
	public abstract void crankItUp();
	public abstract void shutItDown();
	public abstract void placeItem(Object object);
	public abstract Object removeLastItem();
	
	public Queue<Object> getQueue() {
		return queue;
	}
	public void setQueue(Queue<Object> queue) {
		this.queue = queue;
	}
	
}
