package com.starvin.factory.conveyerbelt;

import java.util.concurrent.LinkedBlockingQueue;

import com.starvin.factory.supplier.component.Component;


public class ComponentConveyerBelt extends AbstractConveyerBelt{

	/**
	 * Starts up the conveyer belt with a new empty queue.
	 */
	public void crankItUp(){
		queue = new LinkedBlockingQueue<Object>();
	}

	public void shutItDown(){
		queue = null;
	}
	
	/**
	 * Adds item onto Queue.
	 */
	@Override
	public void placeItem(Object object) {
		
		queue.add(object);
	}

	public Component checkLastItemType(){
		Component component = (Component) queue.peek();
		return component;
	}

	/**
	 * @see com.starvin.factory.conveyerbelt.AbstractConveyerBelt#removeLastItem()
	 * Retrieves and removes the head Object of this queue, or returns null if this queue is empty
	 */
	@Override
	public Object removeLastItem() {
		return queue.poll();
	}
}
