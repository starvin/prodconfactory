package com.starvin.factory.supplier;

import java.util.ArrayList;
import java.util.Random;

import com.starvin.factory.conveyerbelt.ComponentConveyerBelt;
import com.starvin.factory.supplier.component.BroomComponent;
import com.starvin.factory.supplier.component.Component;
import com.starvin.factory.supplier.component.MainComponent;
import com.starvin.factory.supplier.component.MopComponent;

public class ComponentSupplier extends AbstractSupplier implements Runnable {
  
    private ComponentConveyerBelt conveyorBelt;
    private ArrayList<Component> componentsArray;
    		
    Random rand = new Random();

    int MAX_QUEUE_SIZE = 10;
    int resetCounter = 0;
    boolean keepSupplying = true;
    
    public ComponentSupplier(ComponentConveyerBelt conveyorBelt){
        
    	this.conveyorBelt=conveyorBelt;
    	
    	componentsArray = new ArrayList<Component>(); 
    	
    	componentsArray.add(new BroomComponent("BROOM"));
    	componentsArray.add(new MopComponent("MOP"));
    	componentsArray.add(new MainComponent("MAIN"));
    }
    
    
    public void run() {
	    while(keepSupplying){
	    	try {
	        	   
	        	if(conveyorBelt.getQueue().size() < MAX_QUEUE_SIZE)
	   	    	{
	        		supply();
	   	    	}
	        	else{
	        		resetCounter++;
	        	}

	        	Thread.sleep(1000); 
	        	
	        	if(resetCounter == 10){
	        		resetCounter =0;
	        		System.out.println("!!tired of waiting, removing head item!!");
	        		conveyorBelt.removeLastItem();
	        	}
	        	
	           } catch (Exception ex) {
	        	   ex.printStackTrace();
	           }
	    }
    }
 
    public Component getRandomComponent(){
	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((2 - 0) + 1) + 0;
    
    	return componentsArray.get(randomNum);
    }

	private void supply() {
		 
		try{
			Component component =  getRandomComponent();
			System.out.println("Adding "+component.getName()+" to conveyor belt.");
	        getConveyorBelt().placeItem(component);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void startSupplying() {
		Thread prodThread = new Thread(this, "Producer");
		prodThread.start(); 
	}

	public ComponentConveyerBelt getConveyorBelt() {
		return conveyorBelt;
	}

	public void setConveyorBelt(ComponentConveyerBelt conveyorBelt) {
		this.conveyorBelt = conveyorBelt;
	}

	@Override
	protected void stopSupplying() {
		// TODO Auto-generated method stub
	}
}