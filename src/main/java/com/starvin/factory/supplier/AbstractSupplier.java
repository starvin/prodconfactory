package com.starvin.factory.supplier;

public abstract class AbstractSupplier {

	protected abstract void startSupplying();
	protected abstract void stopSupplying();
	
}
