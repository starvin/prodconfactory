package com.starvin.factory.supplier.component;

public abstract class Component {

	public String name;
	
	public Component(String name){
		this.name = name;
	}
	
	public String getName(){return name;}
	
}
