package com.starvin.factory;

public abstract class AbstractFactory {

	protected String factoryName;
	
	public abstract void startFactory();
	
	public abstract void closeFactory();
}
