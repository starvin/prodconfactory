package com.starvin.factory.worker;


import java.util.ArrayList;

import com.starvin.factory.conveyerbelt.ComponentConveyerBelt;
import com.starvin.factory.supplier.component.Component;

public class WetRobotWorker extends AbstractRobotWorker  {
 
    private ComponentConveyerBelt conveyorBelt;
     
    ArrayList<Component> mainComponents = new ArrayList<Component>();
    ArrayList<Component> wetComponents = new ArrayList<Component>();
    
    int noOfMainComponentsReQuired = 1;
    int noOfWetComponentsReQuired = 2;
    int noOfRobotsAssembled = 0;
    int MILLISECS_TO_ASSEMBLE = 3000;
    
    boolean keepWorking = true;
    
    public WetRobotWorker(ComponentConveyerBelt conveyorBelt){
        this.conveyorBelt=conveyorBelt;
    }
    
    /**
     * Consume the components if all component required for this RobotWorker
     * 
     * (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
         
    	while(keepWorking){
	        try {
	
	        	synchronized (conveyorBelt) {
					
   				 Component component = (Component)conveyorBelt.getQueue().peek();
	        			
       			 if(component!= null && componentRequired(component)){
       				  
       				//System.out.println("WET component "+component.getName()+" required.");
     		 		
       				storeComponent((Component)conveyorBelt.removeLastItem());
       			 }
   			}
   			
   			 if(readyToAssemble()){
   				 System.out.println("ASSEMBLING WET ROBOT");   
   				 Thread.sleep(MILLISECS_TO_ASSEMBLE);
   				
   				 mainComponents.clear();
   				 wetComponents.clear();
   				 noOfRobotsAssembled++;
   				 System.out.println("I've made "+noOfRobotsAssembled+" WET robots!");   
   				
   			 }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
    	}
    }
 
    public void startWorking() {
		Thread consumerThread = new Thread(this, "Wet Robot");
		consumerThread.start(); 
	}
    
    private boolean componentRequired(Component component){
    	
    	if(component.getName().equals("MAIN") 
    			&& mainComponents.size() < noOfMainComponentsReQuired){
    		return true;
    	}
    	else if(component.getName().equals("MOP") 
    			&& wetComponents.size() < noOfWetComponentsReQuired){
    		return true;
    	}else{
    		return false;
    		
    	}
    }
    
    /**
     * Return true is all required components have been taken from conveyor belt
     * @return boolean
     */
    private boolean readyToAssemble(){
    	if(mainComponents.size() == noOfMainComponentsReQuired 
    			&& wetComponents.size()== noOfWetComponentsReQuired){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public void storeComponent(Component component){
		if(component.getName().equals("MAIN")){
	    	mainComponents.add(component);
    	}
    	else if(component.getName().equals("MOP")){
    		wetComponents.add(component);	
    	}
	}
}
