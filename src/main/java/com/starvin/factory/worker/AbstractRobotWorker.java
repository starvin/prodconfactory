package com.starvin.factory.worker;


public abstract class AbstractRobotWorker  implements Runnable {

	public abstract void startWorking();
	
}
