package com.starvin.factory.worker;


import java.util.ArrayList;

import com.starvin.factory.conveyerbelt.ComponentConveyerBelt;
import com.starvin.factory.supplier.component.Component;

public class DryRobotWorker extends AbstractRobotWorker {
 
    private ComponentConveyerBelt conveyorBelt;
     
    ArrayList<Component> mainComponents = new ArrayList<Component>();
    ArrayList<Component> dryComponents = new ArrayList<Component>();
    
    int noOfMainComponentsReQuired = 1;
    int noOfDryComponentsReQuired = 2;
    int noOfRobotsAssembled = 0;
    
    boolean keepWorking = true;
    
    int MILLISECS_TO_ASSEMBLE = 3000;
    
    public DryRobotWorker(ComponentConveyerBelt conveyorBelt){
        this.conveyorBelt=conveyorBelt;
    }
    
    
    public void run() {       	
    	while(keepWorking){
    		try {
	        	
    			synchronized (conveyorBelt) {
					
    				 Component component = (Component)conveyorBelt.getQueue().peek();
	        			
        			 if(component!= null && componentRequired(component)){	
        				 storeComponent((Component)conveyorBelt.removeLastItem()); 
        			 }
    			}
    			
    			 if(readyToAssemble()){
    				 System.out.println("ASSEMBLING DRY ROBOT");  
    				 Thread.sleep(MILLISECS_TO_ASSEMBLE);
    				 
    				 mainComponents.clear();
       				 dryComponents.clear();
       				 noOfRobotsAssembled++;
      				 System.out.println("I've made "+noOfRobotsAssembled+" DRY robots!");     
       				
    			 }	
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
    	}
    }
 
    public void startWorking() {
		Thread consumerThread = new Thread(this, "Dry Robot");
		consumerThread.start(); 
	}
    
    private boolean componentRequired(Component component){
    	if(component.getName().equals("MAIN") 
    			&& mainComponents.size() < noOfMainComponentsReQuired){
    		return true;
    	}
    	else if(component.getName().equals("BROOM") 
    			&& dryComponents.size() < noOfDryComponentsReQuired){
    		return true;
    		
    	}else{
    		return false;
    	}
    }
    
	public void storeComponent(Component component){
		if(component.getName().equals("MAIN")){
	    		mainComponents.add(component);
    	}
    	else if(component.getName().equals("BROOM")){
    		dryComponents.add(component);	
    	}
	}

	 
    /**
     * Return true is all required components have been taken from conveyor belt
     * @return boolean
     */
    private boolean readyToAssemble(){
    	if(mainComponents.size() == noOfMainComponentsReQuired 
    			&& dryComponents.size()== noOfDryComponentsReQuired){
    		return true;
    	}else{
    		return false;
    	}
    }
}
